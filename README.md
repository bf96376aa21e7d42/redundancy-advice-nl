# redundancy-advice-nl

This is not legal advice, and I am not a lawyer. 

This document is intended to provide some advice and resources for people working in the Netherlands, who are or may be asked to take a voluntary redundancy, or who are being fired, typically because of "commercial reasons".

It is primarily intended for expats, to help negotiate and navigate through some parts of the process.

## tl;dr

* you have rights
* if you are being asked to take a voluntary redundancy package, you can negotiate, and you can refuse.
* make sure you get things in writing, especially the redundancy offer, and the reason you are being made redundant. Your employer is obligated to justify their reasoning. 
* your employer has the same minimum notice period as you do (typically one or two months)
* you should take the time to process and review any communications from your employer. You do not have to respond immediately, and you can involve a lawyer at any time. 

## Advice

* stay calm, and do not feel obligated to commit to something you are not comfortable with
* whatever your feelings are in relation to your employer, once you have been given notice, you should treat this as a neutral negotiation. It is a zero sum game in which you should prioritise and stand up for your needs - your employer will be prioritising theirs. 
* in general, information available from official resources will reflect the bare minimum your employer must offer you. You should accept nothing less, but can negotiate for more. 

## Voluntary vs involuntary redundancy

### Involuntary

* your employer will need to receive the approval of the [UWV](https://www.uwv.nl/overuwv/english/about-us-executive-board-organization/detail/about-us). It is worth asking for the details of the request to the UWV (specifically, the reason for dismissal).
* the UWV may or may not agree with your employer regarding there being grounds for dismissal. Common business reasons that may be considered by the UWV are documented at https://www.rijksoverheid.nl/onderwerpen/ontslag/afspiegelingsbeginsel
* if the UWV grants your employer's request, you will still have at least a month notice of your dimissal _after the UWV has issued its decision_.
* if the UWV grants your employer's request, you may appeal that decision - https://www.rijksoverheid.nl/onderwerpen/ontslag/vraag-en-antwoord/wat-moet-ik-doen-als-ik-word-ontslagen provides some information about this. 

### Voluntary

* check what you are entitled to, for example using the government's calculator, and do not accept less - https://rekenhulptransitievergoeding.nl/
* take time to consider the offer, and your response to it
* compensation related to voluntary redundancy is often taxed at a different (higher) rate than your salary - take this into account when reviewing any offers, and in any counter-offers you might make.


## Links and resources

The following resources are _indicative_, non-official, and in English. Please note they are essentially opinions:

* overview of severance pay calculation system: https://www.iamsterdam.com/en/work/employment-laws-and-benefits/severance-pay
* overview of unemployment benefits (note that these are not necessarily applicable to your case): https://www.iamsterdam.com/en/work/employment-laws-and-benefits/unemployment-benefits
* ACCESS, an expat-focused organisation that may offer help and/or referrals: https://www.iamsterdam.com/en/living/feel-at-home-in-amsterdam/community/access

The following are official government resources:

* government's FAQ on the process of being made redundant (in Dutch): https://www.rijksoverheid.nl/onderwerpen/ontslag
* government's compensation calculator (in Dutch): https://rekenhulptransitievergoeding.nl/



